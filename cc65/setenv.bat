@echo off
set CC65_PATH=c:\cc65
set PATH=%PATH%;%CC65_PATH%\bin
set CC65_INC=%CC65_PATH%\include
set LD65_LIB=%CC65_PATH%\lib
set LD65_CFG=%CC65_PATH%\cfg
echo CC65_PATH=%CC65_PATH%
echo CC65_INC=%CC65_INC%
echo LD65_LIB=%LD65_LIB%
echo LD65_CFG=%LD65_CFG%
echo.
echo Ready.