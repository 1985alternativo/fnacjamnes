//fnacjamnes for nes
//include the library

#include "neslib.h"
#include "palbg1.h"
#include "palsp1.h"
#include "start.h"
#include "gameover.h" 
#include "tsbgmap.h"
#include "tssprmap1.h"
#include "tssprmap2.h"
#include "tssprmap3.h"
#include "tssprmap4.h"
#include "tssprmap5.h"
#include "tssprmap6.h"
#include "map1.h"

#define PLAYER_BASE_RIGHT 0
#define PLAYER_BASE_LEFT 7
#define PLAYER_OFFSET 0
#define START_X_POSITION 10
#define START_Y_POSITION 130
#define PLAYER_IDDLE 0
#define PLAYER_GOING_UP 1
#define PLAYER_FALLING 2 
#define PLAYER_WALKING 3
#define PLAYER_DEAD 4


#define MAX_BULLETS 6


#define PROP_BASE_RIGHT 0
#define PROP_BASE_LEFT 3

#define ENEMY1_BASE_RIGHT 0
#define ENEMY1_BASE_LEFT 4

#define SCREEN_WIDTH 32
#define SCREEN_HEIGHT 30

#define SCREEN_WIDTH_PIXELS 250
#define MAP_WIDTH 64
#define MAP_HEIGHT 30
#define MAP_WIDTH_BIT 5

#define MIN_SCROLL_VALUE 0
#define MAX_SCROLL_VALUE 255

#define TILE_RELOAD_OFFSET 20

#define TILE_EMPTY 0
#define TILE_SOLID 1
#define TILE_HAZARD 2


#define SFX_START 1
#define SFX_JUMP 2
#define SFX_DEAD 10
#define SFX_PLAYER_SHOOTS 13


//extern variables
extern const unsigned char music_ingame[];
extern const unsigned char music_title[];

//global variables
unsigned char i,j,k;
unsigned int  a,b,c;
unsigned char framecnt;
unsigned char player_base;
unsigned char player_index;
unsigned char player_x;
unsigned char player_y;
unsigned char player_vx;
unsigned char player_vy;
unsigned char player_metatilex1;
unsigned char player_metatilex2;
unsigned char player_metatilex3;
unsigned char player_metatiley1;
unsigned char player_metatiley2;
unsigned char player_status;
unsigned char offset;
unsigned int scroll_x;
unsigned int scroll_y;
unsigned int map2Load;
unsigned char isalive;
unsigned char fader;
unsigned char has2ChangeScreen;

unsigned char bullet_index;
unsigned char bullet_x[MAX_BULLETS];
unsigned char bullet_y[MAX_BULLETS];
unsigned char bullet_direction[MAX_BULLETS];

unsigned int enemy1_x;
unsigned int enemy1_y;
unsigned char enemy1_index;
unsigned char enemy1_direction;
unsigned char prop_base;

unsigned char current_screen;

unsigned char current_metatile;

unsigned char lifenumber;

//sprites
const char *spr_player [] = {
	spr_pl1_00_a, spr_pl1_01_a, spr_pl1_02_a, spr_pl1_03_a, spr_pl1_04_a, spr_pl1_05_a, spr_pl1_06_a,  // Right
	spr_pl1_00_b, spr_pl1_01_b, spr_pl1_02_b, spr_pl1_03_b,	spr_pl1_04_b, spr_pl1_05_b, spr_pl1_06_b// Left
};

const char *spr_props [] = {
	spr_pl2_00_a,spr_pl2_01_a,spr_pl2_02_a,
	spr_pl2_00_b,spr_pl2_01_b,spr_pl2_02_b,
};
const char *spr_enemy1 [] = {
	spr_pl5_00_b,spr_pl5_01_b,spr_pl5_02_b,spr_pl5_03_b,
	spr_pl5_00_a,spr_pl5_01_a,spr_pl5_02_a,spr_pl5_03_a

};

const char *spr_xplos [] = {
	spr_pl4_00_a,spr_pl4_01_a,spr_pl4_02_a,
	spr_pl4_00_b,spr_pl4_01_b,spr_pl4_02_b,
};

const char *mapblocks [] = {
	map1a,map1b,map1c,map1d,map1e,map1f,map1g,map1h,map1i, map1j
};


//map array
unsigned char map[SCREEN_WIDTH*(SCREEN_HEIGHT>>1)];
const unsigned char tileType[64]={
TILE_SOLID,TILE_SOLID,TILE_SOLID,TILE_SOLID,TILE_SOLID,TILE_SOLID,TILE_SOLID,TILE_HAZARD,
TILE_SOLID, TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,
TILE_EMPTY, TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,
TILE_EMPTY, TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,
TILE_EMPTY, TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,
TILE_EMPTY, TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,
TILE_EMPTY, TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,
TILE_EMPTY, TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,TILE_EMPTY,
};

void fade_out (void) { 
	fader = 5; 
	while (fader --) { 
		pal_bright (fader); 
		ppu_waitnmi (); 
	} 
}
	
void fade_in (void) { 
	fader = 5; 
	while (fader --) { 
		pal_bright (4 - fader); 
		ppu_waitnmi (); 
	} 
}

void init_screen(){
	scroll_x = 0;
	scroll_y = 0;
	scroll(scroll_x,scroll_y);
}


void load_screen(){
	//load nametable part 1 
		map2Load = 0x2000;
		vram_adr(map2Load);
		vram_inc(0);
		k = 0;
		
		for(i=0;i<SCREEN_HEIGHT;i++){
			for(j=0;j<(SCREEN_WIDTH>>1);j++){
				current_metatile = mapblocks[current_screen][k];
				current_metatile = current_metatile - 0x01;
				if(!(i & 1)){
					map[((i>>1)<<5)+j] = tileType[current_metatile];
					vram_put(current_metatile<<2);
					vram_put((current_metatile<<2)+2);
				} else {
					vram_put((current_metatile<<2)+1);
					vram_put((current_metatile<<2)+3);
				}
				k++;
			}
			if(!(i & 1)){
				k = k - 16;
			}
		}
		//paleta nametable 1
		k = 0;
		for(i=0;i<(SCREEN_HEIGHT>>2);i++){
			for(j=0;j<(SCREEN_WIDTH>>2);j++){
				current_metatile = (ts_pals[mapblocks[current_screen][k]-1])<<0 | (ts_pals[mapblocks[current_screen][k+1]-1])<<2 | (ts_pals[mapblocks[current_screen][k+16]-1])<<4 | (ts_pals[mapblocks[current_screen][k+16+1]-1])<<6;
				vram_put(current_metatile);
				k = k + 2;
			}
			k = k + 16;
		}
		for(j=0;j<(SCREEN_WIDTH>>2);j++){
				current_metatile = (ts_pals[mapblocks[current_screen][k]-1])<<0 | (ts_pals[mapblocks[current_screen][k+1]-1])<<2;
				vram_put(current_metatile);
				k = k + 2;
		}
		//nametable 2
		map2Load = 0x2400;
		vram_adr(map2Load);
		vram_inc(0);
		k = 0;
		
		for(i=0;i<SCREEN_HEIGHT;i++){
			for(j=0;j<(SCREEN_WIDTH>>1);j++){
				current_metatile = mapblocks[current_screen+1][k];
				current_metatile = current_metatile - 0x01;
				if(!(i & 1)){
					map[((i>>1)<<5)+j+(SCREEN_WIDTH >> 1)] = tileType[current_metatile];
					vram_put(current_metatile<<2);
					vram_put((current_metatile<<2)+2);
				} else {
					vram_put((current_metatile<<2)+1);
					vram_put((current_metatile<<2)+3);
				}
				k++;
			}
			if(!(i & 1)){
				k = k - 16;
			}
		}
		//paleta nametable 2
		k = 0;
		for(i=0;i<(SCREEN_HEIGHT>>2);i++){
			for(j=0;j<(SCREEN_WIDTH>>2);j++){
				//value = (topleft << 0) | (topright << 2) | (bottomleft << 4) | (bottomright << 6
				current_metatile = (ts_pals[mapblocks[current_screen+1][k]-1])<<0 | (ts_pals[mapblocks[current_screen+1][k+1]-1])<<2 | (ts_pals[mapblocks[current_screen+1][k+16]-1])<<4 | (ts_pals[mapblocks[current_screen+1][k+16+1]-1])<<6;
				vram_put(current_metatile);
				k = k + 2;
			}
			k = k + 16;
		}
		for(j=0;j<(SCREEN_WIDTH>>2);j++){
				//value = (topleft << 0) | (topright << 2) | (bottomleft << 4) | (bottomright << 6
				current_metatile = (ts_pals[mapblocks[current_screen+1][k]-1])<<0 | (ts_pals[mapblocks[current_screen+1][k+1]-1])<<2;
				vram_put(current_metatile);
				k = k + 2;
		}
}


void start_screen() {
		//init nes
		bank_spr(1);
		bank_bg (0);
		init_screen();

		ppu_off();
		pal_clear();
		pal_bg(fnacjamnes_bg1);
		pal_spr(fnacjamnes_sp1);
		//load nametable part 1 
		map2Load = 0x2000;
		vram_adr(map2Load);
		vram_inc(0);
		k = 0;
		
		for(i=0;i<SCREEN_HEIGHT;i++){
			for(j=0;j<(SCREEN_WIDTH>>1);j++){
				current_metatile = startscreen[k];
				current_metatile = current_metatile - 0x01;
				if(!(i & 1)){
					vram_put(current_metatile<<2);
					vram_put((current_metatile<<2)+2);
				} else {
					vram_put((current_metatile<<2)+1);
					vram_put((current_metatile<<2)+3);
				}
				k++;
			}
			if(!(i & 1)){
				k = k - 16;
			}
		}
		//paleta nametable 1
		k = 0;
		for(i=0;i<(SCREEN_HEIGHT>>2);i++){
			for(j=0;j<(SCREEN_WIDTH>>2);j++){
				current_metatile = (ts_pals[startscreen[k]-1])<<0 | (ts_pals[startscreen[k+1]-1])<<2 | (ts_pals[startscreen[k+16]-1])<<4 | (ts_pals[startscreen[k+16+1]-1])<<6;
				vram_put(current_metatile);
				k = k + 2;
			}
			k = k + 16;
		}
		for(j=0;j<(SCREEN_WIDTH>>2);j++){
				current_metatile = (ts_pals[startscreen[k]-1])<<0 | (ts_pals[startscreen[k+1]-1])<<2;
				vram_put(current_metatile);
				k = k + 2;
		}
		oam_clear();
		oam_size(0);
		ppu_on_all();
		music_play(music_title);
		fade_in();
		while(1){
			ppu_waitnmi();
			i=pad_poll(0);
			if((i&PAD_START)){
				sfx_play(SFX_START,1);
				music_stop();
				break;
			}
		}
		music_stop();
		fade_out();
}

void main_level() {
	//init nes
	bank_spr(1);
	bank_bg (0);
	lifenumber = 3;
	current_screen = 0;
	//bucle externo
	while(lifenumber){
		ppu_off();
		pal_clear();
		pal_bg(fnacjamnes_bg1);
		pal_spr(fnacjamnes_sp1);

		
		//init player
		player_x = START_X_POSITION;
		player_y = START_Y_POSITION;
		player_vx = 0;
		player_vy = 0;
		player_metatilex1 = (scroll_x + player_x - 1) >> 4;
		player_metatilex2 = (scroll_x + player_x + 8) >> 4;
		player_metatilex3 = (scroll_x + player_x + 16) >> 4;
		player_metatiley1 = player_y >> 4;
		player_metatiley2 = (player_y + 16) >> 4;
		
		player_base = PLAYER_BASE_RIGHT;
		player_index = 0;
		player_status = PLAYER_IDDLE;
		
		//init bullets
		for(bullet_index=0;bullet_index<MAX_BULLETS;bullet_index++){
			bullet_x[bullet_index] = 255;
			bullet_y[bullet_index] = 0;
			bullet_direction[bullet_index] = PLAYER_BASE_RIGHT;
		}
		bullet_index = 0;

		//init enemies
		enemy1_x = 340;
		enemy1_y = 178;
		enemy1_index = 0;
		enemy1_direction = ENEMY1_BASE_RIGHT;
		
		offset = PLAYER_OFFSET;
		
		has2ChangeScreen = 0;
		
		load_screen();

		init_screen();
		scroll_x = 0;
		scroll_y = 0;


		j = 0;
		framecnt = 0;
		//turn on nes
		oam_clear();
		oam_size(0);
		ppu_on_all();
		music_play(music_ingame);
		isalive = 1;
		fade_in();
		while(isalive)
		{
			ppu_waitnmi();
			if(has2ChangeScreen){
				fade_out();
				ppu_off();
				load_screen();
				init_screen();
				ppu_on_all();
				fade_in();
				player_x = 4;
				has2ChangeScreen = 0;
			}
			i=pad_poll(0);
			if(player_status == PLAYER_IDDLE){
				if(map[((player_metatiley2 ) << 5) + player_metatilex2] == TILE_EMPTY){
					framecnt = 0;
					player_vy = 0;
					player_status = PLAYER_FALLING;
				} else {
					if((i&PAD_LEFT) || (i&PAD_RIGHT)){
						framecnt = 0;
						player_status = PLAYER_WALKING;
					} 
					if((i&PAD_A)){
						player_vy = 4;
						player_status = PLAYER_GOING_UP;
						sfx_play(SFX_JUMP,0);
						framecnt = 0;	
					}	
				} 
			} else if(player_status == PLAYER_WALKING) {
				if(map[((player_metatiley2) << 5) + player_metatilex2] == TILE_EMPTY){
					framecnt = 0;
					player_vy = 0;
					player_status = PLAYER_FALLING;
				} else {
					if((i&PAD_LEFT)){
						if(map[((player_metatiley1) << 5) + player_metatilex1] != TILE_EMPTY){
							player_vx = 0;
							//player_x
						} else {
							if(player_base == PLAYER_BASE_RIGHT){
								framecnt = 0;
								player_vx = 0;
								player_base = PLAYER_BASE_LEFT;
							}
							if(framecnt >= 4)
								player_vx = 1;
							if(framecnt >= 8)
								player_vx = 2;
							if(framecnt >= 12)
								player_vx = 3;
								
						}
						
						
					} else if((i&PAD_RIGHT)){
						if(map[((player_metatiley1) << 5) + player_metatilex3] != TILE_EMPTY){
							player_vx = 0;
							//player_x
						} else {
							if(player_base == PLAYER_BASE_LEFT){
								framecnt = 0;
								player_vx = 0;
								player_base = PLAYER_BASE_RIGHT;
							}
							if(framecnt >= 4)
								player_vx = 1;
							if(framecnt >= 8)
								player_vx = 2;
							if(framecnt >= 12)
								player_vx = 3;
						}
					} else {
						if((framecnt & 7) == 1 && player_vx > 0){
							player_vx--;
						}
						if(player_vx == 0){
							framecnt = 0;
							player_status = PLAYER_IDDLE;
						}	
					}
					if((i&PAD_A)){
						player_vy = 4;
						player_status = PLAYER_GOING_UP;
						sfx_play(SFX_JUMP,0);
						framecnt = 0;	
					}	
				}
			} else if(player_status == PLAYER_GOING_UP){
				if((i&PAD_LEFT)){
					if(player_base == PLAYER_BASE_RIGHT){
						player_base = PLAYER_BASE_LEFT;
						player_vx = 1;
					} else if(player_vx == 0){
						player_vx = 1;
					}
				} else if((i&PAD_RIGHT)){
					if(player_base == PLAYER_BASE_LEFT){
						player_base = PLAYER_BASE_RIGHT;
						player_vx = 1;
					} else if(player_vx == 0){
						player_vx = 1;
					}
				}
				if(player_base == PLAYER_BASE_RIGHT){
					if(map[((player_metatiley1) << 5) + player_metatilex3] != TILE_EMPTY)
						player_vx = 0;
				} else {
					if(map[((player_metatiley1) << 5) + player_metatilex1] != TILE_EMPTY)
						player_vx = 0;
				}
				if((framecnt & 7) == 1){
					if(player_vy > 0){
						player_vy--;
					}
					if(player_vy == 0){
						player_status = PLAYER_FALLING;
						framecnt = 0;
					}
				}

			} else if(player_status == PLAYER_FALLING){

				if((map[((player_metatiley2) << 5) + player_metatilex2] == TILE_HAZARD)){
					sfx_play(SFX_DEAD,2);
					player_vy = 0;
					player_vy = player_metatiley1 << 4;
					player_status = PLAYER_DEAD;
					framecnt = 0;
					player_vx = 0;
				} else if((map[((player_metatiley2) << 5) + player_metatilex2] != TILE_SOLID)){
				//if((map[((player_metatiley2 + 1) << 4) + player_metatilex] != TILE_SOLID) || ((player_y & 7) != 0)){
					if((i&PAD_LEFT)){
						if(player_base == PLAYER_BASE_RIGHT){
							player_base = PLAYER_BASE_LEFT;
							player_vx = 1;
						}
					} else if((i&PAD_RIGHT)){
						if(player_base == PLAYER_BASE_LEFT){
							player_base = PLAYER_BASE_RIGHT;
							player_vx = 1;
						}
						
					}
					if(player_base == PLAYER_BASE_RIGHT){
						if(map[((player_metatiley1) << 5) + player_metatilex3] != TILE_EMPTY)
							player_vx = 0;
					} else {
						if(map[((player_metatiley1) << 5) + player_metatilex1] != TILE_EMPTY)
							player_vx = 0;
					}
					if((framecnt & 7) == 1){
						player_vy++;
					}
				} else {
					if(player_vx > 0)
						player_status = PLAYER_WALKING;
					else 
						player_status = PLAYER_IDDLE;
					player_vy = 0;
					player_y = player_metatiley1 << 4;
				}
			}
			//check if you have to fire
			if((i&PAD_B)){
				if(!(framecnt & 7)){
					if(player_base == PLAYER_BASE_RIGHT){
						bullet_x[bullet_index] = player_x + 8;
						bullet_direction[bullet_index] = PLAYER_BASE_RIGHT;	
					} else {
						bullet_x[bullet_index] = player_x - 8;
						bullet_direction[bullet_index] = PLAYER_BASE_LEFT;
					}
					
					bullet_y[bullet_index]	= player_y - 5;
					bullet_index++;
					if(bullet_index >= MAX_BULLETS)
						bullet_index = 0;
					sfx_play(SFX_PLAYER_SHOOTS,1);
				}
			
			}
			//updtae prop base
			if(player_base == PLAYER_BASE_RIGHT){
				prop_base = PROP_BASE_RIGHT;
			} else {
				prop_base = PROP_BASE_LEFT;
			}
			
			//update positions and animation for player
			if(player_base == PLAYER_BASE_RIGHT){
				if(player_x > 140){
					if(scroll_x + player_vx < MAX_SCROLL_VALUE){
						scroll_x = scroll_x + player_vx;	
					} else {
						if(player_x + player_vx < SCREEN_WIDTH_PIXELS){
							player_x = player_x + player_vx;
						} else {
							current_screen = current_screen + 2;
							has2ChangeScreen = 1;
						}
					}		
				} else {
					player_x = player_x + player_vx;	
				}
			} else {
	 			/*if(player_x < 50) {
	 				if(scroll_x - player_vx > MIN_SCROLL_VALUE) {
	 					scroll_x = scroll_x - player_vx;
	 				} else {
	 					scroll_x = 0;
	 					if(player_x > player_vx) {
							player_x = player_x - player_vx;
						} else {
							player_vx = 0;
						}
	 				}
	 			} else {
	 				player_x = player_x - player_vx;
	 			}*/
	 			if(player_x > player_vx){
	 				player_x = player_x - player_vx;
	 			}
				
			}

			if(player_status == PLAYER_FALLING) {
				player_y = player_y + player_vy;
				player_index = 1;
			}
			else if(player_status == PLAYER_GOING_UP) {
				player_y = player_y - player_vy;
				player_index = 5;
			}
			if(player_status == PLAYER_WALKING){
				if((framecnt & ((32>>player_vx) - 1)) == 1){
					player_index++;
					if(player_index > 4)
						player_index = 0;
				}	
			} else if(player_status == PLAYER_IDDLE) {
				player_index = 0;
			} else if(player_status == PLAYER_DEAD) {
				if(framecnt > 60){
					isalive = 0;
					music_stop();
				}
				if(framecnt >= 15 && ((framecnt & 15) == 0)){
					player_index++;
				}
			}
			player_metatilex1 = (scroll_x + player_x - 1) >> 4;
			player_metatilex2 = (scroll_x + player_x + 8) >> 4;
			player_metatilex3 = (scroll_x + player_x + 16) >> 4;
			player_metatiley1 = player_y >> 4;
			player_metatiley2 = (player_y + 16) >> 4;
			
			scroll(scroll_x,scroll_y);
			framecnt++;
			//reset framcnt
			if(framecnt >= 254){
				framecnt = 0;
			}

			//paint player sprites
			offset = PLAYER_OFFSET;
			if(player_status == PLAYER_DEAD && framecnt >= 15){
				offset = oam_meta_spr(player_x+8,player_y,offset,spr_xplos[player_index]);
			} else {
				offset = oam_meta_spr(player_x+8,player_y,offset,spr_player[player_base + player_index]);
			}	
			
			//update positions for bullets and paint them
			for(j=0;j<MAX_BULLETS;j++){
				if (bullet_x[j]<250 && bullet_x[j] > 6)
				{
					if(bullet_direction[j] == PLAYER_BASE_RIGHT){
						bullet_x[j] = bullet_x[j] + 6;
					} else {
						bullet_x[j] = bullet_x[j] - 6;
					}

				} else {
					bullet_x[j] = 255;
					bullet_y[j] = 0;
					bullet_direction[j] = PLAYER_BASE_RIGHT;
				}
				offset = oam_meta_spr(bullet_x[j],bullet_y[j],offset,spr_props[prop_base]);
			}
			//update enemies and paint them 
			if(enemy1_direction == ENEMY1_BASE_RIGHT){
				if(enemy1_x < 460){
					enemy1_x = enemy1_x + 1;	
				} else {
					enemy1_direction = ENEMY1_BASE_LEFT;
				}
				
			} else {
				if(enemy1_x > 340){
					enemy1_x = enemy1_x - 1;
				} else {
					enemy1_direction = ENEMY1_BASE_RIGHT;
				}
			}
			if(enemy1_x > scroll_x)
				a = enemy1_x - scroll_x;
			else
				a = 0;
			if(framecnt&1){
				enemy1_index++;
				if(enemy1_index > 3)
					enemy1_index = 0;	
			}

			if(a<255 && a>0){ 
				offset = oam_meta_spr(a, enemy1_y,offset,spr_enemy1[enemy1_direction+enemy1_index]);
			} else {
				offset = oam_meta_spr(0, 247,offset,spr_enemy1[0]);
			}
			
			//paint lifes
			k = 200;
			for(j=0;j<lifenumber;j++){
				offset = oam_meta_spr(k,20,offset,spr_props[2]);
				k = k + 16;	
			}
			
		}
		fade_out();
		lifenumber--;
	}
	
}

void gameover_screen() {
		//init nes
		bank_spr(1);
		bank_bg (0);
		init_screen();
		ppu_off();
		pal_clear();
		pal_bg(fnacjamnes_bg1);
		pal_spr(fnacjamnes_sp1);
		//load nametable part 1 
		map2Load = 0x2000;
		vram_adr(map2Load);
		vram_inc(0);
		k = 0;
		
		for(i=0;i<SCREEN_HEIGHT;i++){
			for(j=0;j<(SCREEN_WIDTH>>1);j++){
				current_metatile = gameover[k];
				current_metatile = current_metatile - 0x01;
				if(!(i & 1)){
					vram_put(current_metatile<<2);
					vram_put((current_metatile<<2)+2);
				} else {
					vram_put((current_metatile<<2)+1);
					vram_put((current_metatile<<2)+3);
				}
				k++;
			}
			if(!(i & 1)){
				k = k - 16;
			}
		}
		//paleta nametable 1
		k = 0;
		for(i=0;i<(SCREEN_HEIGHT>>2);i++){
			for(j=0;j<(SCREEN_WIDTH>>2);j++){
				current_metatile = (ts_pals[gameover[k]-1])<<0 | (ts_pals[gameover[k+1]-1])<<2 | (ts_pals[gameover[k+16]-1])<<4 | (ts_pals[gameover[k+16+1]-1])<<6;
				vram_put(current_metatile);
				k = k + 2;
			}
			k = k + 16;
		}
		for(j=0;j<(SCREEN_WIDTH>>2);j++){
				current_metatile = (ts_pals[gameover[k]-1])<<0 | (ts_pals[gameover[k+1]-1])<<2;
				vram_put(current_metatile);
				k = k + 2;
		}

		oam_clear();
		oam_size(0);
		ppu_on_all();
		music_play(music_ingame);
		fade_in();
		framecnt = 0;
		while(1){
			ppu_waitnmi();
			framecnt++;
			if(framecnt >= 254)
				break;
		}
		music_stop();
		fade_out();
}

void main(void)
{
	while(1)//infinite loop, title-gameplay
	{
		start_screen();
		main_level();
		gameover_screen();
	}
}