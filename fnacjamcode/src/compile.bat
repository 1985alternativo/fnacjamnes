@echo off

set name="fnacjamnes.nes"

set path=%path%;..\..\cc65\bin\

set CC65_HOME=..\..\cc65\

echo Generating pals
..\utils\mkts.exe mode=pals pals=.\gfx\plbg1.png out=palbg1.h label=fnacjamnes_bg1 silent
..\utils\mkts.exe mode=pals pals=.\gfx\plsp1.png out=palsp1.h label=fnacjamnes_sp1 silent

echo Exporting chr
rem charset, tileset, fill to 4096
..\utils\mkts.exe mode=coltiles in=.\gfx\tsbg1.png pals=.\gfx\plbg1.png out=tsbgfull.bin size=8,8 metasize=2,2 tsmap=tsbgmap.h label=ts silent
..\utils\fillto.exe tsbgfull.bin 4096
rem charset, tileset, fill to 4096
..\utils\mkts.exe in=.\gfx\spritesheet.png pals=.\gfx\plsp1.png out=tssp1.bin mode=sprites tsmap=tssprmap1.h offset=0,0 size=7,1 metasize=2,3 sprorg=-8,-8 label=spr_pl1 genflipped silent
..\utils\mkts.exe in=.\gfx\spritesheet.png pals=.\gfx\plsp1.png out=tssp2.bin mode=sprites tsmap=tssprmap2.h offset=14,0 size=1,3 metasize=1,1 sprorg=0,0 tmapoffset=41 label=spr_pl2 genflipped silent
..\utils\mkts.exe in=.\gfx\spritesheet.png pals=.\gfx\plsp1.png out=tssp3.bin mode=sprites tsmap=tssprmap3.h offset=10,3 size=2,1 metasize=3,2 sprorg=-8,0 tmapoffset=44 label=spr_pl3 genflipped silent
..\utils\mkts.exe in=.\gfx\spritesheet.png pals=.\gfx\plsp1.png out=tssp4.bin mode=sprites tsmap=tssprmap4.h offset=0,3 size=3,1 metasize=2,3 sprorg=-8,-8 tmapoffset=56 label=spr_pl4 genflipped silent
..\utils\mkts.exe in=.\gfx\spritesheet.png pals=.\gfx\plsp1.png out=tssp5.bin mode=sprites tsmap=tssprmap5.h offset=0,6 size=4,1 metasize=2,3 sprorg=-8,-8 tmapoffset=74 label=spr_pl5 genflipped silent
..\utils\mkts.exe in=.\gfx\spritesheet.png pals=.\gfx\plsp1.png out=tssp6.bin mode=sprites tsmap=tssprmap6.h offset=0,9 size=1,1 metasize=15,2 sprorg=0,0 tmapoffset=98 label=spr_pl6 genflipped silent
copy /b tssp1.bin + tssp2.bin + tssp3.bin + tssp4.bin + tssp5.bin + tssp6.bin .\tsspfull.bin
..\utils\fillto.exe tsspfull.bin 4096
echo copying tileset
copy /b tsbgfull.bin+tsspfull.bin  .\tileset.chr > nul


echo compiling game
cc65 -Oi game.c --add-source
ca65 crt0.s
ca65 game.s

ld65 -C nes.cfg -o %name% crt0.o game.o runtime.lib

pause

del *.o
del game.s

%name%