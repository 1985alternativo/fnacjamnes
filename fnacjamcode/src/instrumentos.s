music_instruments:
@ins:
	.word @env_default,@env_default,@env_default
	.byte $30,$00
	.word @env_vol2,@env_default,@env_default
	.byte $30,$00
	.word @env_vol0,@env_default,@env_pitch0
	.byte $30,$00
	.word @env_vol8,@env_default,@env_default
	.byte $70,$00
	.word @env_vol9,@env_arp1,@env_default
	.byte $30,$00
	.word @env_vol13,@env_arp7,@env_default
	.byte $30,$00
	.word @env_vol22,@env_default,@env_default
	.byte $30,$00
	.word @env_vol15,@env_default,@env_default
	.byte $30,$00
	.word @env_vol0,@env_arp4,@env_default
	.byte $30,$00
	.word @env_vol5,@env_default,@env_pitch2
	.byte $70,$00
	.word @env_vol6,@env_default,@env_pitch3
	.byte $70,$00
	.word @env_vol7,@env_default,@env_pitch4
	.byte $30,$00
	.word @env_vol10,@env_default,@env_pitch5
	.byte $30,$00
	.word @env_vol16,@env_arp8,@env_default
	.byte $70,$00
	.word @env_vol17,@env_arp9,@env_default
	.byte $70,$00
	.word @env_vol18,@env_default,@env_default
	.byte $b0,$00
@env_default:
	.byte $c0,$7f,$00
@env_vol0:
	.byte $c9,$7f,$00
@env_vol1:
	.byte $cf,$cd,$cb,$c9,$c7,$c5,$c3,$c1,$c0,$7f,$08
@env_vol2:
	.byte $cf,$7f,$00
@env_vol3:
	.byte $c1,$c3,$c4,$c6,$c7,$c8,$c9,$ca,$ca,$cb,$cc,$cc,$cd,$0c,$cc,$cc
	.byte $cb,$ca,$c7,$c1,$7f,$13
@env_vol4:
	.byte $ce,$7f,$00
@env_vol5:
	.byte $cc,$cb,$ca,$ca,$c9,$02,$c8,$02,$c7,$06,$c6,$07,$c5,$09,$c4,$0b
	.byte $c3,$0b,$c2,$11,$c1,$18,$c0,$04,$7f,$17
@env_vol6:
	.byte $c4,$01,$c3,$0b,$c2,$0a,$c1,$11,$c0,$7f,$08
@env_vol7:
	.byte $cb,$ca,$ca,$c9,$03,$c8,$02,$c7,$06,$c6,$07,$c5,$09,$c4,$0b,$c3
	.byte $0b,$c2,$11,$c1,$18,$c0,$04,$7f,$16
@env_vol8:
	.byte $ca,$06,$c7,$04,$ca,$07,$7f,$05
@env_vol9:
	.byte $c7,$7f,$00
@env_vol10:
	.byte $c4,$01,$c3,$0b,$c2,$0a,$c1,$11,$c0,$7f,$08
@env_vol11:
	.byte $c9,$c9,$c8,$c8,$c7,$c7,$c6,$c6,$c5,$c5,$c4,$7f,$0a
@env_vol12:
	.byte $c7,$01,$c6,$01,$c5,$01,$c4,$7f,$06
@env_vol13:
	.byte $ca,$c9,$c7,$c6,$c3,$c1,$c0,$01,$7f,$07
@env_vol14:
	.byte $c5,$c5,$c4,$c4,$c3,$c3,$c2,$c2,$c1,$c1,$c0,$7f,$0a
@env_vol15:
	.byte $c7,$c2,$c0,$01,$7f,$03
@env_vol16:
	.byte $c8,$c8,$c7,$c7,$c6,$c6,$c5,$c5,$c4,$c4,$c3,$c2,$c1,$7f,$0c
@env_vol17:
	.byte $c6,$01,$c5,$01,$c4,$01,$c3,$c3,$c1,$7f,$08
@env_vol18:
	.byte $c4,$7f,$00
@env_vol22:
	.byte $c9,$c8,$c6,$c4,$c2,$c1,$c0,$01,$7f,$07
@env_arp0:
	.byte $c0,$01,$c1,$c2,$7f,$03
@env_arp1:
	.byte $cc,$c0,$7f,$01
@env_arp2:
	.byte $c0,$2b,$7f,$01
@env_arp3:
	.byte $c0,$c0,$c4,$c4,$c7,$c7,$7f,$00
@env_arp4:
	.byte $c0,$04,$bf,$be,$bd,$bc,$bb,$ba,$b9,$b8,$b7,$b6,$b5,$b4,$b3,$b2
	.byte $b1,$b0,$af,$ae,$ad,$ac,$ab,$aa,$a9,$a8,$a8,$a7,$a7,$a6,$a6,$a5
	.byte $a5,$a4,$a4,$a3,$a3,$a2,$a2,$a1,$a1,$7f,$28
@env_arp5:
	.byte $c0,$c0,$c4,$c4,$c7,$c7,$7f,$00
@env_arp6:
	.byte $c0,$c0,$c4,$c4,$c7,$c7,$7f,$00
@env_arp7:
	.byte $c7,$c3,$c0,$bd,$7f,$02
@env_arp8:
	.byte $c0,$c0,$c4,$c4,$c7,$c7,$7f,$00
@env_arp9:
	.byte $c0,$c0,$c4,$c4,$c7,$c7,$7f,$00
@env_pitch0:
	.byte $c0,$0d,$bf,$be,$bd,$bc,$bd,$be,$bf,$c0,$7f,$02
@env_pitch1:
	.byte $c0,$bf,$bf,$c0,$7f,$00
@env_pitch2:
	.byte $c0,$0e,$c1,$01,$c2,$01,$c1,$01,$c0,$01,$7f,$02
@env_pitch3:
	.byte $c0,$0e,$c1,$01,$c2,$01,$c1,$01,$c0,$01,$7f,$02
@env_pitch4:
	.byte $c0,$0e,$c1,$01,$c2,$01,$c1,$01,$c0,$01,$7f,$02
@env_pitch5:
	.byte $c0,$0e,$c1,$01,$c2,$01,$c1,$01,$c0,$01,$7f,$02
